﻿using Autofac;
using Economic.Solutions.FreelanceAccounting.Bll;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BllTests
{
    public class IntegrationTestBase
    {
        protected IContainer _container;

        public IntegrationTestBase()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule(new BllAutofacModule());

            _container = builder.Build();
        }
    }
}
