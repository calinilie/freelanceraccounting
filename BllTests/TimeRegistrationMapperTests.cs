﻿using Economic.Solutions.FreelanceAccounting.Bll.DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;

namespace BllTests
{

    [TestClass]
    public class TimeRegistrationMapperTests : IntegrationTestBase
    {
        ITimeRegistrationMapper _mapper;

        [TestInitialize]
        public void Setup()
        {
            _mapper = _container.Resolve<ITimeRegistrationMapper>();
        }

        [TestCleanup]
        public void Teardown()
        {
            _container.Dispose();
        }

        [TestMethod]
        public void GetTimeRegistrationTest()
        {
            TimeRegistration actual = _mapper.Get(1);
            TimeRegistration expected = new TimeRegistration()
            {
                TimeRegistrationId = 1,
                TaskId = 1,
                InvoiceId = null
            };

            Assert.AreEqual(expected.TimeRegistrationId, actual.TimeRegistrationId, "Database might not be in the correct state");
            Assert.AreEqual(expected.TaskId, actual.TaskId);
            Assert.AreEqual(expected.InvoiceId, actual.InvoiceId);
        }

        [TestMethod]
        public void GetTimeRegistrationTest2()
        {
            TimeRegistration actual = _mapper.Get(0);
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void InsertTimeRegistrationTest()
        {
            TimeRegistration timeRegistration = new TimeRegistration()
            {
                TaskId = 2,
                FreelancerId = 1,
                StartDateTime = DateTime.Now.AddHours(-1),
                EndDateTime = DateTime.Now.AddHours(0.5),
                Description = "This was inserted via integration tests"
            };

            bool actual = _mapper.Insert(timeRegistration) > 0;
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void GetForTaskTest()
        {
            int taskCount = _mapper.GetForTask(1, false).Count;
            Assert.IsTrue(taskCount >= 1);
        }

    }
}
