﻿using Economic.Solutions.FreelanceAccounting.Bll.DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Moq;
using System.Data;
using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;

namespace BllTests
{
    [TestClass]
    public class TaskMapperTests : IntegrationTestBase
    {
        ITaskMapper _mapper;

        [TestInitialize]
        public void Setup()
        {
            _mapper = _container.Resolve<ITaskMapper>();
        }

        [TestCleanup]
        public void Teardown()
        {
            _container.Dispose();
        }

        [TestMethod]
        public void GetTasksForProjectTest()
        {
            var actual = _mapper.GetTasksForProject(0).Count;
            Assert.AreEqual(0, actual);
        }

        [TestMethod]
        public void GetTaskTest()
        {
            Task actual = _mapper.Get(1);

            Task expected = new Task()
            {
                TaskId = 1,
                ProjectId = 1,
                IsCompleted = true
            };

            Assert.AreEqual(expected.TaskId, actual.TaskId, "The database might not be in the correct state");
            Assert.AreEqual(expected.ProjectId, actual.ProjectId);
            Assert.AreEqual(expected.IsCompleted, actual.IsCompleted);
        }

        [TestMethod]
        public void InsertTaskTest()
        {
            Task toInsert = new Task()
            {
                ProjectId = 4,
                Name = "Test Task",
                Description = "Task inserted from integration tests",
                IsCompleted = false
            };

            bool actual = _mapper.Insert(toInsert) > 0;
            Assert.AreEqual(true, actual);
        }

        [TestMethod]
        public void DataRowToModelTest()
        {
            int expected_TaskId = 1001;
            int expected_ProjectId = 1002;
            string expected_Name = "Project Name";
            string expected_Description = "Description";
            bool expected_IsCompleted = false;

            DataTable table = new DataTable();
            table.Columns.Add("TaskId", typeof(int));
            table.Columns.Add("ProjectId_FK", typeof(int));
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Description", typeof(string));
            table.Columns.Add("IsCompleted", typeof(bool));

            DataRow row = table.NewRow();

            row["TaskId"] = expected_TaskId;
            row["ProjectId_FK"] = expected_ProjectId;
            row["Name"] = expected_Name;
            row["Description"] = expected_Description;
            row["IsCompleted"] = expected_IsCompleted;

            Task actual = _mapper.DataRowToModel(row);

            Assert.AreEqual(expected_TaskId, actual.TaskId);
            Assert.AreEqual(expected_ProjectId, actual.ProjectId);
            Assert.AreEqual(expected_Name, actual.Name);
            Assert.AreEqual(expected_Description, actual.Description);
            Assert.AreEqual(expected_IsCompleted, actual.IsCompleted);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DataRowToModelTest2()
        {
            Task actual = _mapper.DataRowToModel(null);
        }
    }
}
