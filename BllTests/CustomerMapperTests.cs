﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Economic.Solutions.FreelanceAccounting.Bll.DataAccess;
using Autofac;
using Economic.Solutions.FreelanceAccounting.Bll;

namespace BllTests
{
    [TestClass]
    public class CustomerMapperTests : IntegrationTestBase
    {
        ICustomerMapper mapper;

        [TestInitialize]
        public void Setup()
        {
            mapper = _container.Resolve<ICustomerMapper>();
        }

        [TestCleanup]
        public void Teardown()
        {
            _container.Dispose();
        }


        [TestMethod]
        public void GetAllCustomersTest()
        {
            Assert.AreEqual(3, mapper.GetAll().Count);
        }

        
    }
}
