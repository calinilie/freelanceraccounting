﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BllTests
{

    [TestClass]
    public class TimeRegistrationTests
    {

        [TestMethod]
        public void DurationPropertyTest()
        {
            TimeRegistration model = new TimeRegistration();
            model.StartDateTime = new DateTime(2015, 3, 8, 9, 0, 0);
            model.EndDateTime = new DateTime(2015, 3, 8, 10, 0, 0);

            TimeSpan span = model.EndDateTime - model.StartDateTime;


            double actual = model.Duration;
            double expected = 1;

            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void DurationPropertyTest2()
        {
            TimeRegistration model = new TimeRegistration();
            model.StartDateTime = new DateTime(2015, 3, 8, 9, 30, 0);
            model.EndDateTime = new DateTime(2015, 3, 8, 10, 0, 0);

            TimeSpan span = model.EndDateTime - model.StartDateTime;


            double actual = model.Duration;
            double expected = 0.5;

            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void DurationPropertyTest3()
        {
            TimeRegistration model = new TimeRegistration();
            model.StartDateTime = new DateTime(2015, 3, 8, 10, 0, 0);
            model.EndDateTime = new DateTime(2015, 3, 8, 10, 0, 0);

            TimeSpan span = model.EndDateTime - model.StartDateTime;


            double actual = model.Duration;
            double expected = 0.5;

            Assert.AreEqual(expected, actual);
        }



        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void DurationPropertyTest4()
        {
            TimeRegistration model = new TimeRegistration();
            model.StartDateTime = new DateTime(2016, 3, 8, 10, 0, 0);
            model.EndDateTime = new DateTime(2015, 3, 8, 10, 0, 0);

            TimeSpan span = model.EndDateTime - model.StartDateTime;


            double actual = model.Duration;
            double expected = 0.5;

            Assert.AreEqual(expected, actual);
        }


        [TestMethod]
        public void DurationPropertyTest5()
        {
            TimeRegistration model = new TimeRegistration();
            model.StartDateTime = new DateTime(2015, 3, 8, 10, 0, 0);
            model.EndDateTime = new DateTime(2015, 3, 9, 10, 0, 0);

            TimeSpan span = model.EndDateTime - model.StartDateTime;


            double actual = model.Duration;
            double expected = 24;

            Assert.AreEqual(expected, actual);
        }

    }
}
