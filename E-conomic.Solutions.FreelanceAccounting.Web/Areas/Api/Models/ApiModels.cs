﻿using Economic.Solutions.FreelanceAccounting.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Economic.Solutions.FreelanceAccounting.Web.Areas.Api.Models
{
    public class ApiModel<T> : ApiModelBase
        where T: IDto
    {
        public ApiModel(T dto)
            : base()
        {
            Dto = dto;
        }

        public T Dto { get; private set; }
    }

    public class ListApiModel<T> : ApiModelBase
        where T : IDto
    {
        public ListApiModel(IList<T> dto)
            :base()
        {
            Dto = dto;
        }
        public IList<T> Dto { get; private set; }
    }


    public abstract class ApiModelBase : IApiModel
    {
        public ApiModelBase()
        {
            Status = new Status()
            {
                StatusCode = (int)StatusCodes.Success,
                StatusDescription = StatusCodes.Success.ToString()

            };
        }

        public Status Status { get; set; }

        public void SetStatus(ModelStateDictionary modelState)
        {
            Status = new Status();
            if (!modelState.IsValid) 
            { 
                Status.StatusCode = (int)StatusCodes.ValidationError;
                Status.StatusDescription = StatusCodes.ValidationError.ToString();
            }

            Status.ValidationErrors = modelState
                .Where(kv => kv.Value.Errors.Count > 0)
                .Select(kv => new ApiValidationErrorModel() 
                { 
                    FieldName = kv.Key,
                    ErrorMessage = kv.Value.Errors.First().ErrorMessage
                }).ToList();
        }
    }

    public enum StatusCodes
    {
        Success = 200,
        ValidationError = 460
    }

    public class Status 
    {
        public int StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public IList<ApiValidationErrorModel> ValidationErrors { get; set; }
    }

    public class ApiValidationErrorModel
    {
        public string FieldName { get; set; }
        public string ErrorMessage { get; set; }
    }

    
}