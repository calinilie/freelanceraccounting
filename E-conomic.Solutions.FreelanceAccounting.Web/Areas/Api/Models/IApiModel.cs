﻿using Economic.Solutions.FreelanceAccounting.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Economic.Solutions.FreelanceAccounting.Web.Areas.Api.Models
{
    public interface IApiModel
    {
        Status Status { get; set; }

        void SetStatus(ModelStateDictionary modelState);
    }
}
