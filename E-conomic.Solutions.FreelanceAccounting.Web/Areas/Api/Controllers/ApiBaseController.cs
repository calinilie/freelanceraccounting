﻿using Economic.Solutions.FreelanceAccounting.Bll.ServiceLayer;
using Economic.Solutions.FreelanceAccounting.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Economic.Solutions.FreelanceAccounting.Web.Areas.Api.Controllers
{
    public class ApiBaseController : Controller
    {
        protected IServices _serviceLayer;
        public ApiBaseController(IServices serviceLayer)
            :base()
        {
            _serviceLayer = serviceLayer;
        }


        public JsonResult LowerCamelCaseJson(object data)
        {
            return new LowerCamelCaseJsonResult(data, JsonRequestBehavior.DenyGet);
        }

        public JsonResult LowerCamelCaseJson(object data, JsonRequestBehavior jsonRequestbehaviour)
        {
            return new LowerCamelCaseJsonResult(data, jsonRequestbehaviour);
        }
    }
}