﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using Economic.Solutions.FreelanceAccounting.Bll.ServiceLayer;
using Economic.Solutions.FreelanceAccounting.Web.Areas.Api.Models;
using Economic.Solutions.FreelanceAccounting.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Economic.Solutions.FreelanceAccounting.Web.Areas.Api.Controllers
{
    public class TimeRegistrationsController : ApiBaseController
    {
        public TimeRegistrationsController(IServices serviceLayer)
            :base(serviceLayer)
        {
        }

        [HttpPost]
        public JsonResult Post(TimeRegistrationDto dto)
        {
            if (!ModelState.IsValid)
            {
                ApiModel<TimeRegistrationDto> retVal = new ApiModel<TimeRegistrationDto>(dto);
                retVal.SetStatus(ModelState);
                return LowerCamelCaseJson(retVal);
            }

            TimeRegistration model = dto.ToModel();
            model = _serviceLayer.TimeRegistrationService.CreateTimeRegistration(model);
            return LowerCamelCaseJson(new ApiModel<TimeRegistrationDto>(
                new TimeRegistrationDto(model)));
        }

        [HttpGet]
        [ActionName("ForTask")]
        public JsonResult GetTimeRegistrationsForTask(int id)
        {
            IList<TimeRegistration> timeRegs = _serviceLayer.TimeRegistrationService.GetTimeRegistrationsForTask(id, true);
            
            IList<TimeRegistrationDto> retVal = timeRegs.Select(t => new TimeRegistrationDto(t)).ToList();

            return LowerCamelCaseJson(new ListApiModel<TimeRegistrationDto>(retVal), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [ActionName("ForProject")]
        public JsonResult GetTimeRegistrationsForProject(int id)
        {
            IList<CompositeTimeRegistrationDto> retVal = new List<CompositeTimeRegistrationDto>();
            Project project = _serviceLayer.ProjectService.GetProject(id);
            project.Tasks = _serviceLayer.TaskService.GetTasksForProject(id);
            foreach (Task task in project.Tasks)
            {
                task.Parent = project;
                task.TimeRegistrations = _serviceLayer.TimeRegistrationService.GetTimeRegistrationsForTask(task.TaskId, true);
                foreach (TimeRegistration timereg in task.TimeRegistrations)
                    retVal.Add(new CompositeTimeRegistrationDto(timereg, task, project));
            }
            

            return LowerCamelCaseJson(new ListApiModel<CompositeTimeRegistrationDto>(retVal), JsonRequestBehavior.AllowGet);
        }
    }
}