﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using Economic.Solutions.FreelanceAccounting.Bll.ServiceLayer;
using Economic.Solutions.FreelanceAccounting.Web.Areas.Api.Models;
using Economic.Solutions.FreelanceAccounting.Web.Models;
using Economic.Solutions.FreelanceAccounting.Web.Utils;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Economic.Solutions.FreelanceAccounting.Web.Areas.Api.Controllers
{
    public class TasksController : ApiBaseController
    {
        public TasksController(IServices serviceLayer)
            :base(serviceLayer)
        {
        }

        [HttpGet]
        public JsonResult Get(int? id)
        {
            //if (id != null)
            //    return Json(_service.);

            return Json("ONE task", JsonRequestBehavior.AllowGet); 
        }


        [ActionName("ForProject")]
        public JsonResult GetTasksForProject(int id)
        {
            IList<Task> tasks = _serviceLayer.TaskService.GetTasksForProject(id);

            IList<TaskDto> dtos = new List<TaskDto>();
            dtos = tasks.Select(t => new TaskDto(t)).ToList();

            ListApiModel<TaskDto> retVal = new ListApiModel<TaskDto>(dtos);
            return LowerCamelCaseJson(retVal, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Post(TaskDto dto)
        {
            if (!ModelState.IsValid)
            {
                ApiModel<TaskDto> retVal = new ApiModel<TaskDto>(dto);
                retVal.SetStatus(ModelState);
                return LowerCamelCaseJson(retVal);
            }
            
            Task model = dto.ToModel();
            dto.TaskId = _serviceLayer.TaskService.CreateTask(model);
            return LowerCamelCaseJson(new ApiModel<TaskDto>(dto));
        }
    }
}