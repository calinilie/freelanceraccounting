﻿using Autofac;
using Autofac.Integration.Mvc;
using Economic.Solutions.FreelanceAccounting.Bll;
using Economic.Solutions.FreelanceAccounting.Web.Utils;
using FluentValidation;
using FluentValidation.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Economic.Solutions.FreelanceAccounting.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);


            // AUTOFAC registration
            ContainerBuilder builder = new ContainerBuilder();

            // register BLL
            builder.RegisterModule(new BllAutofacModule());

            // register MVC
            builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly);

            // register controllers
            builder.RegisterControllers(typeof(MvcApplication).Assembly)
                  .InstancePerRequest();

            //register ValidatiorFactory
            builder.Register<ValidatorFactory>(c=> new ValidatorFactory(c.Resolve<IComponentContext>()));

            //register custom validators
            AssemblyScanner.FindValidatorsInAssembly(typeof(MvcApplication).Assembly)
                .ForEach(result =>
                {
                    builder.RegisterType(result.ValidatorType)
                       .SingleInstance()
                       .As(result.InterfaceType);
                });

            IContainer container = builder.Build();

            // give container to MVC
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            IValidatorFactory factory = container.Resolve<ValidatorFactory>();

            ModelValidatorProviders.Providers.Add(new FluentValidationModelValidatorProvider(factory));
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;
        }
    }
}