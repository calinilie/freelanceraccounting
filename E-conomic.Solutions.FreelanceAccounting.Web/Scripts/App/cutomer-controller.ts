﻿angular
    .module("freelanceAccoutingApp")
    .controller(
    "customerController",
    ["$scope", "$routeParams", "$location", "appManager", "customerService",
        ($scope, $routeParams, $location, appManager, customerService)
            => new Controllers.CustomerController($scope, $routeParams, $location, appManager, customerService)]
    );

module Controllers {

    interface ICustomerControllerScope extends IFreelanceAccountingScope {
        showCustomersView: boolean;
        customerId: number;
    }

    export class CustomerController {

        _scope: ICustomerControllerScope;
        _params: ng.route.IRouteParamsService;
        _locationService: ng.ILocationService;
        _customerService: Services.ICustomerService;
        _appManager: IFreelanceAccountingAppManager;

        constructor($scope: ICustomerControllerScope,
            $routeParams: ng.route.IRouteParamsService,
            $location: ng.ILocationService,
            appManager: IFreelanceAccountingAppManager,
            customerService: Services.ICustomerService) {

            this._scope = $scope;
            this._locationService = $location;
            this._customerService = customerService;
            this._appManager = appManager;

            this._scope.showCustomersView = true;
            this._scope.context = new Context();
            this._scope.context.setupContext(appManager, $routeParams);

            this.getCustomers();
        }

        getCustomers() {
            var self: CustomerController = this;
            this._customerService.getCustomers().then(
                function (result) {
                    var freshCustomers: Models.Customer[] = result;
                    angular.forEach(
                        freshCustomers,
                        (v, i) => { v.clickUrl = "#" + self._locationService.path() + v.customerId + "/projects/"; });

                    self._appManager.setCustomers(freshCustomers);
                    self._scope.context.customers = freshCustomers;

                },
                function (result) {  /*TODO handle error*/ });
        }

    }
} 