angular.module("freelanceAccoutingApp").controller("customerController", ["$scope", "$routeParams", "$location", "appManager", "customerService", function ($scope, $routeParams, $location, appManager, customerService) { return new Controllers.CustomerController($scope, $routeParams, $location, appManager, customerService); }]);
var Controllers;
(function (Controllers) {
    var CustomerController = (function () {
        function CustomerController($scope, $routeParams, $location, appManager, customerService) {
            this._scope = $scope;
            this._locationService = $location;
            this._customerService = customerService;
            this._appManager = appManager;
            this._scope.showCustomersView = true;
            this._scope.context = new Context();
            this._scope.context.setupContext(appManager, $routeParams);
            this.getCustomers();
        }
        CustomerController.prototype.getCustomers = function () {
            var self = this;
            this._customerService.getCustomers().then(function (result) {
                var freshCustomers = result;
                angular.forEach(freshCustomers, function (v, i) {
                    v.clickUrl = "#" + self._locationService.path() + v.customerId + "/projects/";
                });
                self._appManager.setCustomers(freshCustomers);
                self._scope.context.customers = freshCustomers;
            }, function (result) {
            });
        };
        return CustomerController;
    })();
    Controllers.CustomerController = CustomerController;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=cutomer-controller.js.map