angular.module("freelanceAccoutingApp").controller("taskController", ["$scope", "$routeParams", "$location", "appManager", "taskService", function ($scope, $routeParams, $location, appManager, taskService) { return new Controllers.TaskController($scope, $routeParams, $location, appManager, taskService); }]);
var Controllers;
(function (Controllers) {
    var TaskController = (function () {
        function TaskController($scope, $routeParams, $location, appManager, taskService) {
            this._scope = $scope;
            this._locationService = $location;
            this._taskService = taskService;
            this._appManager = appManager;
            this._scope.showTasksView = true;
            this._scope.showProjectsView = true;
            this._scope.showCustomersView = true;
            this._scope.showValidationError = false;
            this._scope.showCreateTaskButton = true;
            this._scope.context = new Context();
            this._scope.context.setupContext(appManager, $routeParams);
            this.getTasks(this._scope.context.currentProjectId);
            this.resetTaskModel();
            var self = this;
            this._scope.showCreateTask = function showCreateTask() {
                self._scope.showCreateTaskForm = true;
            };
            this._scope.hideCreateTask = function hideCreateTask() {
                self._scope.showCreateTaskForm = false;
            };
            this._scope.createNewTask = function () {
                self._scope.showValidationError = false;
                self._taskService.createTask(self._scope.newTaskModel).then(function (result) {
                    if (result.data.status.validationErrors == null) {
                        result.data.dto.clickUrl = computeClickUrlForTask(self._locationService.path(), result.data.dto.taskId);
                        self._scope.context.addTask(result.data.dto);
                        self.resetTaskModel();
                        self._scope.hideCreateTask();
                    }
                    else {
                        self._scope.showValidationError = true;
                        self._scope.validationErrorMessage = result.data.status.validationErrors[0].errorMessage;
                    }
                }, function (result) {
                });
            };
        }
        TaskController.prototype.getTasks = function (projectId) {
            var self = this;
            self._scope.loadingTasks = true;
            this._taskService.getTasksForProject(projectId).then(function (result) {
                var freshTasks = result.data.dto;
                angular.forEach(freshTasks, function (t) {
                    t.clickUrl = computeClickUrlForTask(self._locationService.path(), t.taskId);
                });
                self._appManager.setTasks(freshTasks);
                self._scope.context.tasks = freshTasks;
                self._scope.noTasks = freshTasks.length == 0;
                self._scope.loadingTasks = false;
            }, function (result) {
            });
        };
        TaskController.prototype.resetTaskModel = function () {
            var self = this;
            self._scope.newTaskModel = new Models.Task();
            self._scope.newTaskModel.projectId = self._scope.context.currentProjectId;
            self._scope.showValidationError = false;
        };
        return TaskController;
    })();
    Controllers.TaskController = TaskController;
})(Controllers || (Controllers = {}));
function computeClickUrlForTask(path, taskId) {
    return "#" + path + taskId + "/timeRegistrations/";
}
//# sourceMappingURL=task-controller.js.map