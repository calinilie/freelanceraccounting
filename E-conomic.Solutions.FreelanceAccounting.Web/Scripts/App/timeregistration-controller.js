angular.module("freelanceAccoutingApp").controller("timeRegistrationController", ["$scope", "$routeParams", "$location", "appManager", "timeRegistrationService", function ($scope, $routeParams, $location, appManager, timeRegistrationService) { return new Controllers.TimeRegistrationController($scope, $routeParams, $location, appManager, timeRegistrationService); }]);
var Controllers;
(function (Controllers) {
    var TimeRegistrationController = (function () {
        function TimeRegistrationController($scope, $routeParams, $location, appManager, timeRegistrationService) {
            this._scope = $scope;
            this._locationService = $location;
            this._timeRegistrationService = timeRegistrationService;
            this._appManager = appManager;
            this._scope.showTimeRegistrationsView = true;
            this._scope.showTasksView = true;
            this._scope.showProjectsView = true;
            this._scope.showCustomersView = true;
            this._scope.showCreateTaskButton = false;
            this._scope.context = new Context();
            this._scope.context.setupContext(appManager, $routeParams);
            this.getTimeRegistrations(this._scope.context.currentTaskId);
            this.resetNewTimeRegModel();
            var self = this;
            this._scope.showCreateTimeReg = function () {
                self._scope.showCreateTimeRegForm = true;
            };
            this._scope.hideCreateTimeReg = function () {
                self._scope.showCreateTimeRegForm = false;
            };
            this._scope.createNewTimeReg = function () {
                self._scope.showValidationError = false;
                self._timeRegistrationService.createTimeRegistration(self._scope.newTimeRegModel).then(function (result) {
                    if (result.data.status.validationErrors == null) {
                        self._scope.context.addTimeRegistration(result.data.dto);
                        self.resetNewTimeRegModel();
                        self._scope.hideCreateTimeReg();
                    }
                    else {
                        self._scope.showValidationError = true;
                        self._scope.validationErrorMessage = result.data.status.validationErrors[0].errorMessage;
                    }
                }, function (result) {
                });
            };
        }
        TimeRegistrationController.prototype.getTimeRegistrations = function (taskId) {
            var self = this;
            self._scope.loadingTimeRegs = true;
            this._timeRegistrationService.getTimeRegistrationsForTask(taskId).then(function (result) {
                var freshTimeRegistrations = result.data.dto;
                self._scope.context.timeRegistrations = freshTimeRegistrations;
                self._scope.noTimeRegs = freshTimeRegistrations.length == 0;
                self._scope.loadingTimeRegs = false;
            }, function (result) {
            });
        };
        TimeRegistrationController.prototype.resetNewTimeRegModel = function () {
            this._scope.newTimeRegModel = new Models.TimeRegistration();
            this._scope.newTimeRegModel.taskId = this._scope.context.currentTaskId;
            this._scope.newTimeRegModel.freelancerId = 1;
            this._scope.showValidationError = false;
        };
        return TimeRegistrationController;
    })();
    Controllers.TimeRegistrationController = TimeRegistrationController;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=timeregistration-controller.js.map