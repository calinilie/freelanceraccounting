﻿module Models {

    class ModelBase {
        isCurrent: boolean;
        clickUrl: string;
    }

    export class Customer extends ModelBase{
        constructor(id: number, name: string) {
            super();

            this.customerId = id;
            this.name = name;
        }

        name: string;
        customerId: number;


    }

    export class Project extends ModelBase{
        constructor(id: number, customerId: number, name: string) {
            super();

            this.name = name;
            this.projectId = id;
            this.customerId = customerId;
        }

        name: string;
        projectId: number;
        customerId: number
    }

    export class Task extends ModelBase{
        taskId: number;
        projectId: number;
        name: string;
        description: string;
        isCompleted: boolean;
    }

    export class TaskListApiModel {
        dto: Task[];
        status: ApiCallStatus;
    }

    export class TaskApiModel {
        dto: Task;
        status: ApiCallStatus;
    }

    export class TimeRegistration {
        timeRegistrationId: number;
        taskId: number;
        freelancerId: number;
        invoiceId: number;
        startDateTime: string;
        endDateTime: string;
        description: string;
        documentedProgressUrl: string;
    }

    export class TimeRegistrationListApiModel {
        dto: TimeRegistration[];
        status: ApiCallStatus;
    }

    export class TimeRegistrationApiModel {
        dto: TimeRegistration;
        status: ApiCallStatus;
    }

    export class ApiCallStatus {
        statusCode: number;
        statusDescription: string;
        validationErrors: ValidationError[];
    }

    export class ValidationError {
        fieldName: string;
        errorMessage: string;
    }
}