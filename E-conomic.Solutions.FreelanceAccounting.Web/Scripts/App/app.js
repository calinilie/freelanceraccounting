var freelanceAccountingApp = angular.module("freelanceAccoutingApp", ["ngRoute"]);
var RouteConfig = (function () {
    function RouteConfig($routeProvider) {
        $routeProvider.when("/customers/", {
            controller: "customerController",
            templateUrl: "view.html"
        }).when("/customers/:customerId/projects/", {
            controller: "projectController",
            templateUrl: "view.html"
        }).when("/customers/:customerId/projects/:projectId/tasks/", {
            controller: "taskController",
            templateUrl: "view.html"
        }).when("/customers/:customerId/projects/:projectId/tasks/:taskId/timeRegistrations", {
            controller: "timeRegistrationController",
            templateUrl: "view.html"
        }).otherwise({ redirectTo: "/customers/" });
    }
    return RouteConfig;
})();
freelanceAccountingApp.config(["$routeProvider", function ($routeProvider) { return new RouteConfig($routeProvider); }]);
var Context = (function () {
    function Context() {
    }
    Context.prototype.addTask = function (task) {
        this.tasks.push(task);
    };
    Context.prototype.addTimeRegistration = function (timeReg) {
        this.timeRegistrations.push(timeReg);
    };
    Context.prototype.setupContext = function (appManager, $routeParams) {
        var _this = this;
        this.customers = appManager.getCustomers();
        if (isDefined(this.customers)) {
            this.currentCustomerId = $routeParams["customerId"];
            this.customers.forEach(function (cus) {
                if (cus.customerId == _this.currentCustomerId)
                    cus.isCurrent = true;
                else
                    cus.isCurrent = false;
            });
        }
        this.projects = appManager.getProjects();
        if (isDefined(this.projects)) {
            this.currentProjectId = $routeParams["projectId"];
            this.projects.forEach(function (prj) {
                if (prj.projectId == _this.currentProjectId)
                    prj.isCurrent = true;
                else
                    prj.isCurrent = false;
            });
        }
        this.tasks = appManager.getTasks();
        if (isDefined(this.tasks)) {
            this.currentTaskId = $routeParams["taskId"];
            this.tasks.forEach(function (task) {
                if (task.taskId == _this.currentTaskId)
                    task.isCurrent = true;
                else
                    task.isCurrent = false;
            });
        }
    };
    return Context;
})();
var AppManager = (function () {
    function AppManager() {
    }
    AppManager.prototype.getCustomers = function () {
        return this._customers;
    };
    AppManager.prototype.setCustomers = function (customers) {
        this._customers = customers;
    };
    AppManager.prototype.getProjects = function () {
        return this._projects;
    };
    AppManager.prototype.setProjects = function (projects) {
        this._projects = projects;
    };
    AppManager.prototype.getTasks = function () {
        return this._tasks;
    };
    AppManager.prototype.setTasks = function (tasks) {
        this._tasks = tasks;
    };
    return AppManager;
})();
freelanceAccountingApp.factory("appManager", [function () { return new AppManager(); }]);
function isDefined(param) {
    return (typeof param !== "undefined");
}
function isValid(status) {
    return !isDefined(this.validationErrors) || this.validationErrors.length == 0;
}
//# sourceMappingURL=app.js.map