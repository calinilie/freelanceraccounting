angular.module("freelanceAccoutingApp").factory("customerService", ["$q", function ($q) { return new Services.CustomerService($q); }]);
angular.module("freelanceAccoutingApp").factory("projectService", ["$q", function ($q) { return new Services.ProjectService($q); }]);
angular.module("freelanceAccoutingApp").factory("taskService", ["$http", function ($http) { return new Services.TaskService($http); }]);
angular.module("freelanceAccoutingApp").factory("timeRegistrationService", ["$http", function ($http) { return new Services.TimeRegistrationService($http); }]);
var Services;
(function (Services) {
    var TimeRegistrationService = (function () {
        function TimeRegistrationService($http) {
            this._http = $http;
        }
        TimeRegistrationService.prototype.getTimeRegistrationsForTask = function (taskId) {
            return this._http.get("/api/timeregistrations/Fortask/" + taskId);
        };
        TimeRegistrationService.prototype.createTimeRegistration = function (model) {
            return this._http.post("/api/timeregistrations/post", model);
        };
        return TimeRegistrationService;
    })();
    Services.TimeRegistrationService = TimeRegistrationService;
    var TaskService = (function () {
        function TaskService($http) {
            this._http = $http;
        }
        TaskService.prototype.getTasksForProject = function (projectId) {
            return this._http.get("/api/tasks/forproject/" + projectId);
        };
        TaskService.prototype.createTask = function (model) {
            return this._http.post("/api/tasks/post", model);
        };
        return TaskService;
    })();
    Services.TaskService = TaskService;
    var CustomerService = (function () {
        function CustomerService($q) {
            this._q = $q;
        }
        CustomerService.prototype.getCustomers = function () {
            var deferred = this._q.defer();
            deferred.resolve([
                new Models.Customer(1, "E-conomic"),
                new Models.Customer(2, "Johnny's Pizza"),
                new Models.Customer(3, "Shidted ApS")
            ]);
            return deferred.promise;
        };
        return CustomerService;
    })();
    Services.CustomerService = CustomerService;
    var ProjectService = (function () {
        function ProjectService($q) {
            this._q = $q;
            this._projects = [
                new Models.Project(1, 1, "Homework Assignment"),
                new Models.Project(2, 1, "Homework Assignment deployment on AWS"),
                new Models.Project(3, 2, "Accounting App"),
                new Models.Project(4, 2, "Testing Purposes project")
            ];
        }
        ProjectService.prototype.getProjectsForCustomer = function (customerProjectId) {
            var deferred = this._q.defer();
            deferred.resolve(this._projects.filter(function (p) { return p.customerId == customerProjectId; }));
            return deferred.promise;
        };
        return ProjectService;
    })();
    Services.ProjectService = ProjectService;
})(Services || (Services = {}));
//# sourceMappingURL=services.js.map