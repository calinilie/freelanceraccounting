﻿angular
    .module("freelanceAccoutingApp")
    .factory("customerService", ["$q", ($q) => new Services.CustomerService($q)]);

angular
    .module("freelanceAccoutingApp")
    .factory("projectService", ["$q", ($q) => new Services.ProjectService($q)]);

angular
    .module("freelanceAccoutingApp")
    .factory("taskService", ["$http", ($http) => new Services.TaskService($http)]);


angular
    .module("freelanceAccoutingApp")
    .factory("timeRegistrationService", ["$http", ($http) => new Services.TimeRegistrationService($http)]);

module Services {


    export interface ITimeRegistrationService{
        getTimeRegistrationsForTask(taskId: number): ng.IHttpPromise<Models.TimeRegistrationListApiModel>;
        createTimeRegistration(model: Models.TimeRegistration): ng.IHttpPromise<Models.TimeRegistrationApiModel>;
    }

    export class TimeRegistrationService implements ITimeRegistrationService{

        private _http: ng.IHttpService;

        constructor($http: ng.IHttpService) {
            this._http = $http;
        }

        getTimeRegistrationsForTask(taskId: number): ng.IHttpPromise<Models.TimeRegistrationListApiModel> {
            return this._http.get("/api/timeregistrations/Fortask/" + taskId);
        }

        createTimeRegistration(model: Models.TimeRegistration): ng.IHttpPromise<Models.TimeRegistrationApiModel> {
            return this._http.post("/api/timeregistrations/post", model);
        }
    }

    export interface ITaskService {
        getTasksForProject(projectId: number): ng.IHttpPromise<Models.TaskListApiModel>;
        createTask(model: Models.Task): ng.IHttpPromise<Models.TaskApiModel>;
    }

    export class TaskService implements ITaskService {
        private _http: ng.IHttpService;

        constructor($http: ng.IHttpService) {
            this._http = $http;
        }

        getTasksForProject(projectId: number): ng.IHttpPromise<Models.TaskListApiModel> {
            return this._http.get("/api/tasks/forproject/" + projectId);
        }

        createTask(model: Models.Task): ng.IHttpPromise<Models.TaskApiModel> {
            return this._http.post("/api/tasks/post", model);
        }
    }

    export interface ICustomerService {
        
        getCustomers(): ng.IPromise<Models.Customer[]>;

    }

    export class CustomerService implements ICustomerService {

        private _q: ng.IQService;

        constructor($q: ng.IQService) {
            this._q = $q;
        }

        getCustomers(): ng.IPromise<Models.Customer[]> {
            var deferred = this._q.defer<Models.Customer[]>();
            deferred.resolve([
                new Models.Customer(1, "E-conomic"),
                new Models.Customer(2, "Johnny's Pizza"),
                new Models.Customer(3, "Shidted ApS")]);
            return deferred.promise;
        }

    }

    export interface IProjectService {
        getProjectsForCustomer(customerId: number): ng.IPromise<Models.Project[]>;
    }

    export class ProjectService implements IProjectService {
        private _q: ng.IQService;

        private _projects: Models.Project[];

        constructor($q: ng.IQService) {
            this._q = $q;
            this._projects = [
                new Models.Project(1, 1, "Homework Assignment"),
                new Models.Project(2, 1, "Homework Assignment deployment on AWS"),
                new Models.Project(3, 2, "Accounting App"),
                new Models.Project(4, 2, "Testing Purposes project")
            ];
        }

        getProjectsForCustomer(customerProjectId: number): ng.IPromise<Models.Project[]> {
            var deferred = this._q.defer<Models.Project[]>();
            deferred.resolve(this._projects.filter(p => p.customerId == customerProjectId));
            return deferred.promise;
        }
    }


}


