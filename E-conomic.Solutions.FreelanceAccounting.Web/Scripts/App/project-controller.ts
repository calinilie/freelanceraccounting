﻿angular
    .module("freelanceAccoutingApp")
    .controller(
    "projectController",
    ["$scope", "$routeParams", "$location", "appManager", "projectService",
        ($scope, $routeParams, $location, appManager, projectService) => new Controllers.ProjectController($scope, $routeParams, $location, appManager, projectService)]
    );

module Controllers {
    interface IProjectControllerScope extends IFreelanceAccountingScope {
        showProjectsView: boolean;
        showCustomersView: boolean;
        projectId: number;
    }

    export class ProjectController {

        _scope: IProjectControllerScope;
        _locationService: ng.ILocationService;
        _appManager: IFreelanceAccountingAppManager;
        _projectService: Services.IProjectService;

        constructor($scope: IProjectControllerScope,
            $routeParams: ng.route.IRouteParamsService,
            $location: ng.ILocationService,
            appManager: IFreelanceAccountingAppManager,
            projectService: Services.IProjectService) {

            this._scope = $scope;
            this._locationService = $location;
            this._appManager = appManager;
            this._projectService = projectService;

            this._scope.showCustomersView = true;
            this._scope.showProjectsView = true;

            this._scope.context = new Context();
            this._scope.context.setupContext(appManager, $routeParams);

            this.getProjectsForCustomer($routeParams["customerId"]);
        }

        getProjectsForCustomer(customerId: number) {
            var self: ProjectController = this;
            this._projectService.getProjectsForCustomer(customerId).then(
                function (result) {
                    var projects: Models.Project[] = result;

                    angular.forEach(projects, prj => {
                        prj.clickUrl = "#" + self._locationService.path() + prj.projectId + "/tasks/";
                    });

                    self._scope.context.projects = projects;
                    self._appManager.setProjects(projects);
                },
                function (result) { /* TODO HANDLE ERROR */ });
        }
    }
}