﻿var freelanceAccountingApp = angular.module("freelanceAccoutingApp", ["ngRoute"]);


class RouteConfig {
    constructor($routeProvider: ng.route.IRouteProvider) {
        $routeProvider
            .when("/customers/", {
            controller: "customerController",
            templateUrl: "view.html"
        })
            .when("/customers/:customerId/projects/", {
            controller: "projectController",
            templateUrl: "view.html"
        })
            .when("/customers/:customerId/projects/:projectId/tasks/", {
            controller: "taskController",
            templateUrl: "view.html"
        })
            .when("/customers/:customerId/projects/:projectId/tasks/:taskId/timeRegistrations", {
            controller: "timeRegistrationController",
            templateUrl: "view.html"
        })
            .otherwise({ redirectTo: "/customers/" });
    }
}

freelanceAccountingApp.config(["$routeProvider", ($routeProvider) => new RouteConfig($routeProvider)]);

class Context {
    currentCustomerId: number;
    currentProjectId: number;
    currentTaskId: number;
    currentFreelancerId: number;
    customers: Models.Customer[];
    projects: Models.Project[];
    tasks: Models.Task[];
    timeRegistrations: Models.TimeRegistration[];

    addTask(task: Models.Task): void {
        this.tasks.push(task);
    }

    addTimeRegistration(timeReg: Models.TimeRegistration): void {
        this.timeRegistrations.push(timeReg);
    }

    setupContext(appManager: IFreelanceAccountingAppManager, $routeParams: ng.route.IRouteParamsService) {
        this.customers = appManager.getCustomers();
        if (isDefined(this.customers)) {
            this.currentCustomerId = $routeParams["customerId"];
            this.customers.forEach(cus => {
                if (cus.customerId == this.currentCustomerId)
                    cus.isCurrent = true;
                else
                    cus.isCurrent = false;
            });
        }

        this.projects = appManager.getProjects();
        if (isDefined(this.projects)) {
            this.currentProjectId = $routeParams["projectId"];
            this.projects.forEach(prj => {
                if (prj.projectId == this.currentProjectId)
                    prj.isCurrent = true;
                else
                    prj.isCurrent = false;
            });
        }

        this.tasks = appManager.getTasks();
        if (isDefined(this.tasks)) {
            this.currentTaskId = $routeParams["taskId"];
            this.tasks.forEach(task => {
                if (task.taskId == this.currentTaskId)
                    task.isCurrent = true;
                else
                    task.isCurrent = false;
            });
        }
    }
}


interface IFreelanceAccountingScope extends ng.IScope {
    context: Context;
    controllerName: string;
}

/*
class AppController {

    private _scope: IFreelanceAccountingScope;
    private _location: ng.route.IRoute;


    constructor($scope: IFreelanceAccountingScope) {
        this._scope = $scope;

        this._scope.controllerName = "AppController";
    }
}

freelanceAccountingApp.controller("appController", ["$scope", "$location", ($scope, $location) => new AppController($scope)]);*/


interface IFreelanceAccountingAppManager {
    getCustomers(): Models.Customer[];
    setCustomers(customers: Models.Customer[]);

    getProjects(): Models.Project[];
    setProjects(projects: Models.Project[]);

    getTasks(): Models.Task[];
    setTasks(tasks: Models.Task[]);
}

class AppManager implements IFreelanceAccountingAppManager{

    private _customers: Models.Customer[];
    getCustomers(): Models.Customer[]{
        return this._customers;
    }

    setCustomers(customers: Models.Customer[]) {
        this._customers = customers;
    }

    private _projects: Models.Project[];
    getProjects(): Models.Project[]{
        return this._projects;
    }
    setProjects(projects: Models.Project[]) {
        this._projects = projects;
    }

    private _tasks: Models.Task[];
    getTasks(): Models.Task[]{
        return this._tasks;
    }
    setTasks(tasks: Models.Task[]) {
        this._tasks = tasks;
    }
}

freelanceAccountingApp.factory("appManager", [() => new AppManager()]);


function isDefined(param) {
    return (typeof param !== "undefined");
}

function isValid(status: Models.ApiCallStatus) {

    return !isDefined(this.validationErrors) || this.validationErrors.length == 0;
}
