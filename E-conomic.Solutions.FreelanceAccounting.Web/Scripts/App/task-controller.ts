﻿angular
    .module("freelanceAccoutingApp")
    .controller(
    "taskController",
    ["$scope", "$routeParams", "$location", "appManager", "taskService",
        ($scope, $routeParams, $location, appManager, taskService)
            => new Controllers.TaskController($scope, $routeParams, $location, appManager, taskService)]
    );


module Controllers {

    interface ITaskControllerScope extends IFreelanceAccountingScope {
        showTasksView: boolean;
        showCustomersView: boolean;
        showProjectsView: boolean;

        noTasks: boolean;
        loadingTasks: boolean;

        showCreateTaskButton: boolean;
        showCreateTaskForm: boolean;
        showCreateTask(): void;
        hideCreateTask(): void;

        newTaskModel: Models.Task;
        createNewTask(): void;

        showValidationError: boolean;
        validationErrorMessage: string;
    }

    export class TaskController {

        _scope: ITaskControllerScope;
        _params: ng.route.IRouteParamsService;
        _locationService: ng.ILocationService;
        _taskService: Services.ITaskService;
        _appManager: IFreelanceAccountingAppManager;

        constructor($scope: ITaskControllerScope,
            $routeParams: ng.route.IRouteParamsService,
            $location: ng.ILocationService,
            appManager: IFreelanceAccountingAppManager,
            taskService: Services.ITaskService) {

            this._scope = $scope;
            this._locationService = $location;
            this._taskService = taskService;
            this._appManager = appManager;

            this._scope.showTasksView = true;
            this._scope.showProjectsView = true;
            this._scope.showCustomersView = true;
            this._scope.showValidationError = false;
            this._scope.showCreateTaskButton = true;

            this._scope.context = new Context();
            this._scope.context.setupContext(appManager, $routeParams);

            this.getTasks(this._scope.context.currentProjectId);

            this.resetTaskModel();

            var self: TaskController = this;
            this._scope.showCreateTask = function showCreateTask() {
                self._scope.showCreateTaskForm = true;
            }
            this._scope.hideCreateTask = function hideCreateTask() {
                self._scope.showCreateTaskForm = false;
            }


            this._scope.createNewTask = function () {
                self._scope.showValidationError = false;
                self._taskService.createTask(self._scope.newTaskModel).then(
                    function (result) {
                        if (result.data.status.validationErrors == null) {
                            result.data.dto.clickUrl = computeClickUrlForTask(self._locationService.path(), result.data.dto.taskId);
                            self._scope.context.addTask(result.data.dto);

                            self.resetTaskModel();
                            self._scope.hideCreateTask();
                        }
                        else {
                            self._scope.showValidationError = true;
                            self._scope.validationErrorMessage = result.data.status.validationErrors[0].errorMessage;
                        }
                    },
                    function (result) { /* TODO Handle Error*/ });
            }
        }

        getTasks(projectId: number) {
            var self: TaskController = this;
            self._scope.loadingTasks = true;

            this._taskService.getTasksForProject(projectId).then(
                function (result) {
                    var freshTasks: Models.Task[] = result.data.dto;
                    angular.forEach(
                        freshTasks,
                        t => { t.clickUrl = computeClickUrlForTask(self._locationService.path(), t.taskId) });

                    self._appManager.setTasks(freshTasks);
                    self._scope.context.tasks = freshTasks;

                    self._scope.noTasks = freshTasks.length == 0;
                    self._scope.loadingTasks = false;
                },
                function (result) {  /*TODO handle error*/ });
        }

        resetTaskModel() {
            var self: TaskController = this;
            self._scope.newTaskModel = new Models.Task();
            self._scope.newTaskModel.projectId = self._scope.context.currentProjectId;
            self._scope.showValidationError = false;
        }
        
    }
} 

function computeClickUrlForTask(path: string, taskId: number): string {
    return "#" + path + taskId + "/timeRegistrations/";
}