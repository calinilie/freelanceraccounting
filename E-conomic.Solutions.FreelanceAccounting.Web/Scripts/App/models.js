var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var Models;
(function (Models) {
    var ModelBase = (function () {
        function ModelBase() {
        }
        return ModelBase;
    })();
    var Customer = (function (_super) {
        __extends(Customer, _super);
        function Customer(id, name) {
            _super.call(this);
            this.customerId = id;
            this.name = name;
        }
        return Customer;
    })(ModelBase);
    Models.Customer = Customer;
    var Project = (function (_super) {
        __extends(Project, _super);
        function Project(id, customerId, name) {
            _super.call(this);
            this.name = name;
            this.projectId = id;
            this.customerId = customerId;
        }
        return Project;
    })(ModelBase);
    Models.Project = Project;
    var Task = (function (_super) {
        __extends(Task, _super);
        function Task() {
            _super.apply(this, arguments);
        }
        return Task;
    })(ModelBase);
    Models.Task = Task;
    var TaskListApiModel = (function () {
        function TaskListApiModel() {
        }
        return TaskListApiModel;
    })();
    Models.TaskListApiModel = TaskListApiModel;
    var TaskApiModel = (function () {
        function TaskApiModel() {
        }
        return TaskApiModel;
    })();
    Models.TaskApiModel = TaskApiModel;
    var TimeRegistration = (function () {
        function TimeRegistration() {
        }
        return TimeRegistration;
    })();
    Models.TimeRegistration = TimeRegistration;
    var TimeRegistrationListApiModel = (function () {
        function TimeRegistrationListApiModel() {
        }
        return TimeRegistrationListApiModel;
    })();
    Models.TimeRegistrationListApiModel = TimeRegistrationListApiModel;
    var TimeRegistrationApiModel = (function () {
        function TimeRegistrationApiModel() {
        }
        return TimeRegistrationApiModel;
    })();
    Models.TimeRegistrationApiModel = TimeRegistrationApiModel;
    var ApiCallStatus = (function () {
        function ApiCallStatus() {
        }
        return ApiCallStatus;
    })();
    Models.ApiCallStatus = ApiCallStatus;
    var ValidationError = (function () {
        function ValidationError() {
        }
        return ValidationError;
    })();
    Models.ValidationError = ValidationError;
})(Models || (Models = {}));
//# sourceMappingURL=models.js.map