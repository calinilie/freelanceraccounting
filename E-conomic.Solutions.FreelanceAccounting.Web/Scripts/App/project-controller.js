angular.module("freelanceAccoutingApp").controller("projectController", ["$scope", "$routeParams", "$location", "appManager", "projectService", function ($scope, $routeParams, $location, appManager, projectService) { return new Controllers.ProjectController($scope, $routeParams, $location, appManager, projectService); }]);
var Controllers;
(function (Controllers) {
    var ProjectController = (function () {
        function ProjectController($scope, $routeParams, $location, appManager, projectService) {
            this._scope = $scope;
            this._locationService = $location;
            this._appManager = appManager;
            this._projectService = projectService;
            this._scope.showCustomersView = true;
            this._scope.showProjectsView = true;
            this._scope.context = new Context();
            this._scope.context.setupContext(appManager, $routeParams);
            this.getProjectsForCustomer($routeParams["customerId"]);
        }
        ProjectController.prototype.getProjectsForCustomer = function (customerId) {
            var self = this;
            this._projectService.getProjectsForCustomer(customerId).then(function (result) {
                var projects = result;
                angular.forEach(projects, function (prj) {
                    prj.clickUrl = "#" + self._locationService.path() + prj.projectId + "/tasks/";
                });
                self._scope.context.projects = projects;
                self._appManager.setProjects(projects);
            }, function (result) {
            });
        };
        return ProjectController;
    })();
    Controllers.ProjectController = ProjectController;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=project-controller.js.map