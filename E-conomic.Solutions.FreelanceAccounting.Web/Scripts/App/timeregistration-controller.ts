﻿angular
    .module("freelanceAccoutingApp")
    .controller(
    "timeRegistrationController",
    ["$scope", "$routeParams", "$location", "appManager", "timeRegistrationService",
        ($scope, $routeParams, $location, appManager, timeRegistrationService)
            => new Controllers.TimeRegistrationController($scope, $routeParams, $location, appManager, timeRegistrationService)]
    );


module Controllers {

    interface ITimeRegistrationControllerScope extends IFreelanceAccountingScope {
        showTimeRegistrationsView: boolean;
        showTasksView: boolean;
        showCustomersView: boolean;
        showProjectsView: boolean;

        noTimeRegs: boolean;
        loadingTimeRegs: boolean;

        showCreateTaskButton: boolean;

        newTimeRegModel: Models.TimeRegistration;
        createNewTimeReg(): void;

        showCreateTimeReg(): void;
        hideCreateTimeReg(): void;
        showCreateTimeRegForm: boolean;

        showValidationError: boolean;
        validationErrorMessage: string;
    }

    export class TimeRegistrationController {

        _scope: ITimeRegistrationControllerScope;
        _params: ng.route.IRouteParamsService;
        _locationService: ng.ILocationService;
        _timeRegistrationService: Services.ITimeRegistrationService;
        _appManager: IFreelanceAccountingAppManager;

        constructor($scope: ITimeRegistrationControllerScope,
            $routeParams: ng.route.IRouteParamsService,
            $location: ng.ILocationService,
            appManager: IFreelanceAccountingAppManager,
            timeRegistrationService: Services.ITimeRegistrationService) {

            this._scope = $scope;
            this._locationService = $location;
            this._timeRegistrationService = timeRegistrationService;
            this._appManager = appManager;

            this._scope.showTimeRegistrationsView = true;
            this._scope.showTasksView = true;
            this._scope.showProjectsView = true;
            this._scope.showCustomersView = true;
            this._scope.showCreateTaskButton = false;

            this._scope.context = new Context();
            this._scope.context.setupContext(appManager, $routeParams);

            this.getTimeRegistrations(this._scope.context.currentTaskId);

            this.resetNewTimeRegModel();

            var self: TimeRegistrationController = this;

            this._scope.showCreateTimeReg = function () {
                self._scope.showCreateTimeRegForm = true;
            }

            this._scope.hideCreateTimeReg = function () {
                self._scope.showCreateTimeRegForm = false;
            }

            this._scope.createNewTimeReg = function () {
                self._scope.showValidationError = false;
                self._timeRegistrationService.createTimeRegistration(self._scope.newTimeRegModel).then(
                    function (result) {
                        if (result.data.status.validationErrors == null) {
                            self._scope.context.addTimeRegistration(result.data.dto);
                            self.resetNewTimeRegModel();

                            self._scope.hideCreateTimeReg();
                        }
                        else {
                            self._scope.showValidationError = true;
                            self._scope.validationErrorMessage = result.data.status.validationErrors[0].errorMessage;
                        }
                    },
                    function (result) { /* TODO handle error*/ });
            }
        }

        getTimeRegistrations(taskId: number) {
            var self: TimeRegistrationController = this;
            self._scope.loadingTimeRegs = true;

            this._timeRegistrationService.getTimeRegistrationsForTask(taskId).then(
                function (result) {
                    var freshTimeRegistrations: Models.TimeRegistration[] = result.data.dto;

                    self._scope.context.timeRegistrations = freshTimeRegistrations;

                    self._scope.noTimeRegs = freshTimeRegistrations.length == 0;
                    self._scope.loadingTimeRegs = false;
                },
                function (result) {  /*TODO handle error*/ });
        }

        resetNewTimeRegModel() {
            this._scope.newTimeRegModel = new Models.TimeRegistration();
            this._scope.newTimeRegModel.taskId = this._scope.context.currentTaskId;
            this._scope.newTimeRegModel.freelancerId = 1;
            this._scope.showValidationError = false;
        }

    }
} 