﻿using Autofac;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Economic.Solutions.FreelanceAccounting.Web.Utils
{
    public class ValidatorFactory : ValidatorFactoryBase {

        private IComponentContext _componentContext;

        public ValidatorFactory(IComponentContext context)
        {
            _componentContext = context;
        }

        public override IValidator CreateInstance(Type validatorType)
        {
            return _componentContext.Resolve(validatorType) as IValidator;
        }
    }
}