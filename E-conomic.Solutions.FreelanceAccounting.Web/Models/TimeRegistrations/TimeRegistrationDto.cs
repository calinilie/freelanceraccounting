﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Economic.Solutions.FreelanceAccounting.Web.Models
{
    public class TimeRegistrationDto : DtoBase<TimeRegistration>
    {
        public TimeRegistrationDto()
        {
        }

        public TimeRegistrationDto(TimeRegistration model)
            :base(model)
        {
        }

        public int TimeRegistrationId { get; set; }
        public int TaskId { get; set; }
        public int FreelancerId { get; set; }
        public int? InvoiceId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Description { get; set; }
        public string DocumentedProgressUrl { get; set; }
        public double Duration { get; set; }

        public override void SetupFromModel(TimeRegistration model)
        {
            TimeRegistrationId = model.TimeRegistrationId;
            TaskId = model.TaskId;
            FreelancerId = model.FreelancerId;
            InvoiceId = model.InvoiceId;
            StartDateTime = model.StartDateTime;
            EndDateTime = model.EndDateTime;
            Description = model.Description;
            DocumentedProgressUrl = model.DocumentedProgressUrl;
            Duration = model.Duration;
        }

        public override TimeRegistration ToModel()
        {
            return new TimeRegistration()
            {
                TimeRegistrationId = TimeRegistrationId,
                TaskId = TaskId,
                FreelancerId = FreelancerId,
                InvoiceId = InvoiceId,
                StartDateTime = StartDateTime,
                EndDateTime = EndDateTime,
                Description = Description,
                DocumentedProgressUrl = DocumentedProgressUrl
            };
        }
    }

    public class TimeRegistrationValidator : AbstractValidator<TimeRegistrationDto>
    {
        public TimeRegistrationValidator()
        {
            RuleFor(t => t.TaskId).NotEmpty();
            RuleFor(t => t.FreelancerId).NotEmpty();
            RuleFor(t => t.StartDateTime).NotEmpty();
            RuleFor(t => t.EndDateTime).NotEmpty().GreaterThan(t => t.StartDateTime);
            RuleFor(t => t.Description).NotEmpty().Length(0, 100);
            RuleFor(t => t.DocumentedProgressUrl).Length(0, 2048);
        }
    }
}