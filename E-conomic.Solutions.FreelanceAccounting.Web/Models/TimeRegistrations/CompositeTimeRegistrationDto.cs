﻿using CuttingEdge.Conditions;
using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Economic.Solutions.FreelanceAccounting.Web.Models
{
    public class CompositeTimeRegistrationDto : CompositeDtoBase
    {
        private static string CLASS_NAME { get { return System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName; } }


        public CompositeTimeRegistrationDto()
        {
        }

        public CompositeTimeRegistrationDto(params IModel[] models)
            : base(models)
        {
        }

        public int TimeRegistrationId { get; set; }
        public double Duration { get; set; }
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }

        public override void SetupFromModels(params IModel[] models)
        {
            TimeRegistration timeReg = models.Where(t => t is TimeRegistration).SingleOrDefault() as TimeRegistration;
            Condition.Requires(timeReg).IsNotNull(string.Format("A {0} requireds a {1} ", CLASS_NAME, timeReg.GetType().Name));

            Project project = models.Where(t => t is Project).SingleOrDefault() as Project;
            Condition.Requires(timeReg).IsNotNull(string.Format("A {0} requireds a {1} ", CLASS_NAME, project.GetType().Name));

            Task task = models.Where(t => t is Task).SingleOrDefault() as Task;
            Condition.Requires(timeReg).IsNotNull(string.Format("A {0} requireds a {1} ", CLASS_NAME, task.GetType().Name));


            TimeRegistrationId = timeReg.TimeRegistrationId;
            Duration = timeReg.Duration;

            TaskId = task.TaskId;
            TaskName = task.Name;

            ProjectId = project.ProjectId;
            ProjectName = project.Name;

        }
    }
}