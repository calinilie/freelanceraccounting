﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Economic.Solutions.FreelanceAccounting.Web.Models
{
    public interface IFormViewModel<T>
        where T: IDto
    {
        T Dto { get; }
    }

    public class ViewModelBase
    {

    }

    public class FormViewModelBase<T> : ViewModelBase, IFormViewModel<T>
        where T : IDto
    {
        public FormViewModelBase(T dto)
        {
            Dto = dto;
        }

        public T Dto { get; private set; }
    }
}