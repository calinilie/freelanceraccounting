﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Economic.Solutions.FreelanceAccounting.Web.Models
{
    public abstract class DtoBase<T> : IDto
        where T : IModel
    {
        public DtoBase()
        {

        }

        public DtoBase(T model)
        {
            SetupFromModel(model);
        }

        public abstract void SetupFromModel(T model);

        public abstract T ToModel();
    }

    public abstract class CompositeDtoBase : IDto
    {
        public CompositeDtoBase()
        {

        }

        public CompositeDtoBase(params IModel[] models)
        {
            SetupFromModels(models);
        }

        public abstract void SetupFromModels(params IModel[] models);
    }
}