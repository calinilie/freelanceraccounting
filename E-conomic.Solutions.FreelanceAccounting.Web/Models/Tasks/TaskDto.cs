﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Economic.Solutions.FreelanceAccounting.Web.Models
{
    public class TaskDto : DtoBase<Task>
    {
        public TaskDto()
        { 
        }

        public TaskDto(Task t)
            : base(t)
        {
        }

        public int TaskId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsCompleted { get; set; }
        public int ProjectId { get; set; }

        public override void SetupFromModel(Task model)
        {
            TaskId = model.TaskId;
            Name = model.Name;
            Description = model.Description;
            IsCompleted = model.IsCompleted;
            ProjectId = model.ProjectId;
        }

        public override Task ToModel()
        {
            return new Task()
            {
                TaskId = TaskId,
                Name = Name,
                Description = Description,
                IsCompleted = IsCompleted,
                ProjectId = ProjectId
            };
        }
    }

    public class TaskValidator : AbstractValidator<TaskDto>
    {
        public TaskValidator()
        {
            RuleFor(task => task.Name).NotEmpty().Length(0, 50);
            RuleFor(task => task.ProjectId).NotEmpty();
            RuleFor(task => task.Description).Length(0, 300);
        }
    }
}