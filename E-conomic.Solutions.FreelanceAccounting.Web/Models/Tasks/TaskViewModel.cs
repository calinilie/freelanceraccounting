﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Economic.Solutions.FreelanceAccounting.Web.Models
{
    /// <summary>
    /// THIS IS NOT USED ANYWHERE -> Dto Models and ApiModels are used
    /// </summary>
    public class TaskViewModel : FormViewModelBase<TaskDto>
    {
        public TaskViewModel(TaskDto dto)
            :base(dto)
        {

        }

    }
}