﻿using Autofac;
using Economic.Solutions.FreelanceAccounting.Bll.DataAccess;
using Economic.Solutions.FreelanceAccounting.Bll.ServiceLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll
{
    public class BllAutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //CONNECTION FACTORY
            //the connection string should be in a .config file
            builder.Register<SqlConnectionFactory>(c => new SqlConnectionFactory(@"Server=ec2-46-137-191-120.eu-west-1.compute.amazonaws.com\BETA,1434;Database=FreelanceAccounting;User ID=Freelancer;Password=lionsonthetable;"))
                .As<ISqlConnectionFactory>();

            //DATA MAPPERS
            builder.Register<CustomerMapper>(c => new CustomerMapper(c.Resolve<ISqlConnectionFactory>()))
                .As<ICustomerMapper>();

            builder.Register<ProjectMapper>(c => new ProjectMapper(c.Resolve<ISqlConnectionFactory>()))
                .As<IProjectMapper>();

            builder.Register<TaskMapper>(c => new TaskMapper(c.Resolve<ISqlConnectionFactory>()))
                .As<ITaskMapper>();

            builder.Register<TimeRegistrationMapper>(c => new TimeRegistrationMapper(c.Resolve<ISqlConnectionFactory>()))
                .As<ITimeRegistrationMapper>();

            builder.Register<DataMappers>(c => new DataMappers(c.Resolve<BllAutofacResolver>()))
                .As<IDataMappers>();

            //REGISTER SERVICES
            builder.Register<CustomerService>(c => new CustomerService(c.Resolve<IDataMappers>()))
                .As<ICustomerService>();

            builder.Register<ProjectService>(c => new ProjectService(c.Resolve<IDataMappers>()))
                .As<IProjectService>();

            builder.Register<TaskService>(c => new TaskService(c.Resolve<IDataMappers>()))
                .As<ITaskService>();

            builder.Register<TimeRegistrationService>(c => new TimeRegistrationService(c.Resolve<IDataMappers>()))
                .As<ITimeRegistrationService>();

            builder.Register<IServices>(c => new Services(c.Resolve<BllAutofacResolver>()))
                .As<IServices>();

            //REGISTER RESOLVER
            builder.Register<BllAutofacResolver>(c => new BllAutofacResolver(c.Resolve<IComponentContext>()));

        }
    }
}
