﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DomainModel
{
    public class Customer : ModelBase<Freelancer>
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public decimal HourlyRate { get; set; }
    }
}
