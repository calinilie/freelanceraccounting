﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Economic.Solutions.FreelanceAccounting.Bll.DomainModel
{
    public class TimeRegistration : IModel
    {
        public int TimeRegistrationId { get; set; }
        public int TaskId { get; set; }
        public int FreelancerId { get; set; }
        public int? InvoiceId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Description { get; set; }
        public string DocumentedProgressUrl { get; set; }

        public IModel Parent { get; set; }

        /// <summary>
        /// duration in hours
        /// </summary>
        public double Duration
        {
            get
            {
                TimeSpan span = EndDateTime - StartDateTime;
                double retVal = span.TotalHours;

                if (retVal <= 0)
                    throw new ArgumentException("Invalid data: Duration of a TimeRegistration cannot be null. TimeRegistration: " + TimeRegistrationId);

                return retVal;
            }
        }
    }
}
