﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DomainModel
{
    public class Freelancer : ModelBase<IModel>
    {
        public Freelancer()
        {

        }

        public int Id { get; set; }
        public string EmailAddress { get; set; }
        
    }
}
