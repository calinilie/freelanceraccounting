﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DomainModel
{
    public abstract class ModelBase<T> : IModel
        where T : IModel
    {
        public virtual T Parent { get; set; }
        IModel IModel.Parent { get { return Parent; } }
    }
}
