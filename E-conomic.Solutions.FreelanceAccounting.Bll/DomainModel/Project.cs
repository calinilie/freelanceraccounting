﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DomainModel
{
    public class Project : ModelBase<Customer>
    {
        public int ProjectId { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }

        public IList<Task> Tasks { get; set; }
    }
}
