﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DomainModel
{
    public interface IModel
    {
        IModel Parent { get; }
    }

    /*public static class IModelExtensions
    {
        public static T GetNearestAncestorByType<T>(this IModel instance)
            where T : class, IModel
        {
            if (instance == null)
                return null;

            T retVal = instance as T;

            if (retVal != null)
                return retVal;

            if (instance.Parent == null)
                return null;

            return instance.Parent.GetNearestAncestorByType<T>();
        }
    }*/
}
