﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public interface IDataMapper<T>
        where T : class, IModel
    {
        T Get(int id);
        int Insert(T data);
        T DataRowToModel(DataRow row);
    }
}
