﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public interface ITimeRegistrationMapper : IDataMapper<TimeRegistration>
    {
        IList<TimeRegistration> GetForTask(int taskId, bool nonInvoicedOnly);
    }
}
