﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public class TimeRegistrationMapper : DataMapperBase<TimeRegistration>, ITimeRegistrationMapper
    {
        public TimeRegistrationMapper(ISqlConnectionFactory connectionFactory)
            : base(connectionFactory)
        {

        }

        public override TimeRegistration Get(int id)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = @"
                    select *
                    from TimeRegistrations
                    where TimeRegistrationId = @timeRegId";

            cmd.Parameters.Add("@timeRegId", SqlDbType.Int).Value = id;
            return SelectObject(cmd);
        }

        public override int Insert(TimeRegistration data)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"
                    INSERT INTO TimeRegistrations
                        (TaskId_FK, FreelancerId_FK, InvoiceId_FK, StartDateTime, EndDateTime, Description, DocumentedProgressUrl) 
                    values
                        (@TaskId_FK, @FreelancerId_FK, @InvoiceId_FK, @StartDateTime, @EndDateTime, @Description, @DocumentedProgressUrl) 
                    SELECT @@IDENTITY as NewRecordId ";

            cmd.Parameters.Add("@TaskId_FK", SqlDbType.Int).Value = data.TaskId;
            cmd.Parameters.Add("@FreelancerId_FK", SqlDbType.Int).Value = data.FreelancerId;
            cmd.Parameters.Add("@InvoiceId_FK", SqlDbType.Int).Value = Nullable(data.InvoiceId);
            cmd.Parameters.Add("@StartDateTime", SqlDbType.DateTime).Value = data.StartDateTime;
            cmd.Parameters.Add("@EndDateTime", SqlDbType.DateTime).Value = data.EndDateTime;
            cmd.Parameters.Add("@Description", SqlDbType.NVarChar, 140).Value = Nullable(data.Description);
            cmd.Parameters.Add("@DocumentedProgressUrl", SqlDbType.NVarChar, 2048).Value = Nullable(data.DocumentedProgressUrl);

            DataRow row = SelectSingleRow(cmd);
            return Convert.ToInt32(row["NewRecordId"]);
        }

        public IList<TimeRegistration> GetForTask(int taskId, bool nonInvoicedOnly)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"
                    select *
                    from TimeRegistrations
                    where TaskId_FK = @taskId";

            if (nonInvoicedOnly)
                cmd.CommandText += " and InvoiceId_FK is NULL";

            cmd.Parameters.Add("@taskId", SqlDbType.Int).Value = taskId;
            return SelectObjects(cmd);
        }

        public override TimeRegistration DataRowToModel(DataRow row)
        {
            TimeRegistration retVal = new TimeRegistration();

            retVal.TimeRegistrationId = base.DbRead<int>(row["TimeRegistrationId"]);
            retVal.TaskId = base.DbRead<int>(row["TaskId_FK"]);
            retVal.FreelancerId = base.DbRead<int>(row["FreelancerId_FK"]);
            retVal.InvoiceId = base.DbRead<int?>(row["InvoiceId_FK"]);
            retVal.StartDateTime = base.DbRead<DateTime>(row["StartDateTime"]);
            retVal.EndDateTime = base.DbRead<DateTime>(row["EndDateTime"]);
            retVal.Description = base.DbRead<string>(row["Description"]);
            retVal.DocumentedProgressUrl = base.DbRead<string>(row["DocumentedProgressUrl"]);

            return retVal;
        }
    }
}
