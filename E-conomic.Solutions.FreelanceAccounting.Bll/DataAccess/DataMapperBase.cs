﻿using CuttingEdge.Conditions;
using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public abstract class DataMapperBase<T> : IDataMapper<T>
        where T : class, IModel
    {
        private ISqlConnectionFactory _connectionFactory;

        public DataMapperBase(ISqlConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public abstract T Get(int id);

        public abstract int Insert(T data);

        public abstract T DataRowToModel(DataRow row);

#region reading utility helpers

        protected T SelectObject(SqlCommand command)
        {
            IList<T> models = SelectObjects(command);
            if (models.Count == 0)
                return null;

            return models[0];
        }

        protected IList<T> SelectObjects(SqlCommand command)
        {
            return DataTableToList(Select(command));
        }

        protected fieldT DbRead<fieldT>(object fieldValue)
        {
            if (fieldValue == DBNull.Value)
                return default(fieldT);
            else
                return (fieldT)fieldValue;
        }

        protected DataTable Select(SqlCommand command)
        {
            Condition.Requires<SqlCommand>(command, "SqlCommand")
                .IsNotNull<SqlCommand>();

            using (SqlConnection connection = _connectionFactory.GetConnection())
            using (command)
            {
                try
                {
                    command.Connection = connection;
                    SqlDataReader reader = command.ExecuteReader();
                    DataTable data = new DataTable();
                    data.Load(reader);
                    return data;
                }
                catch (Exception ex)
                {
                    //TODO Log exception
                    throw ex;
                }
            }
        }

        protected int ExecuteNonQuery(SqlCommand command)
        {
            using (SqlConnection connection = _connectionFactory.GetConnection())
            using (command)
            {
                try
                {
                    command.Connection = connection;
                    return command.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    //TODO log exception
                    throw e;
                }
            }
        }

        protected DataRow SelectSingleRow(SqlCommand command)
        {
            DataTable dt = Select(command);

            // Post-condition
            Condition.Ensures<DataTable>(dt)
                .IsNotNull<DataTable>("Select(SqlCommand) returned 'null'.");

            if (dt.Rows.Count > 0)
                return dt.Rows[0];
            else
                return null;
        }

        protected object Nullable(object value)
        {
            if (value == null)
                return DBNull.Value;
            else
                return value;
        }

        private IList<T> DataTableToList(DataTable dataTable)
        {
            IList<T> retVal = new List<T>();

            if (dataTable == null)
                return retVal;

            foreach (DataRow row in dataTable.Rows)
            {
                T model = DataRowToModel(row);
                retVal.Add(DataRowToModel(row));
            }

            return retVal;
        }

#endregion reading utility helpers

    }
}
