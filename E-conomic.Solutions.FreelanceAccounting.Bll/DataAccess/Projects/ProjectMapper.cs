﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public class ProjectMapper : DataMapperBase<Project>, IProjectMapper
    {
        public ProjectMapper(ISqlConnectionFactory connectionFactory)
            : base(connectionFactory)
        {

        }

        public IList<Project> GetProjectsForCustomer(int customerId)
        {
            return _projects.Where(p => p.CustomerId == customerId).ToList();
        }

        public override Project Get(int id)
        {
            return _projects.Where(p => p.ProjectId == id).SingleOrDefault();
        }

        public override int Insert(Project data)
        {
            throw new NotImplementedException();
        }

        public override Project DataRowToModel(System.Data.DataRow row)
        {
            throw new NotImplementedException();
        }

        private static IList<Project> _projects = new List<Project>
        {
            new Project()
            {
                ProjectId = 1,
                CustomerId = 1,
                Name = "Homework Assignment"
            },
            new Project()
            {
                ProjectId = 2,
                CustomerId = 1,
                Name = "Homework Assignment deployment on AWS"
            },
            new Project()
            {
                ProjectId = 3,
                CustomerId = 2,
                Name = "Accounting App"
            },
            new Project()
            {
                ProjectId = 4,
                CustomerId = 2,
                Name = "Testing purposes project"
            }
        };
    }
}
