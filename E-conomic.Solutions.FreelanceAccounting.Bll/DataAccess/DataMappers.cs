﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public interface IDataMappers
    {
        ICustomerMapper CustomerMapper { get; }
        IProjectMapper ProjectMapper { get; }
        ITaskMapper TaskMapper { get; }
        ITimeRegistrationMapper TimeRegistrationMapper { get; }
    }

    public class DataMappers : IDataMappers
    {
        private BllAutofacResolver _resolver;

        public DataMappers(BllAutofacResolver resolver)
        {
            _resolver = resolver;
        }

        public ICustomerMapper CustomerMapper { get { return _resolver.Resolve<ICustomerMapper>(); } }

        public IProjectMapper ProjectMapper { get { return _resolver.Resolve<IProjectMapper>(); } }

        public ITaskMapper TaskMapper { get { return _resolver.Resolve<ITaskMapper>(); } }

        public ITimeRegistrationMapper TimeRegistrationMapper { get { return _resolver.Resolve<ITimeRegistrationMapper>(); } }
    }
}
