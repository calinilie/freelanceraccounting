﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public class CustomerMapper : DataMapperBase<Customer>, ICustomerMapper
    {
        public CustomerMapper(ISqlConnectionFactory connectionFactory)
            : base(connectionFactory)
        {

        }

        public IList<Customer> GetAll()
        {
            return _customers;
        }

        public override Customer Get(int id)
        {
            return _customers.Single(c => c.CustomerId == id);
        }

        public override int Insert(Customer data)
        {
            throw new NotImplementedException();
        }

        public override Customer DataRowToModel(System.Data.DataRow row)
        {
            throw new NotImplementedException();
        }

        private static IList<Customer> _customers = new List<Customer>
        {
            new Customer()
            {
                CustomerId = 1,
                Name = "E-conomic"
            },
            new Customer()
            {
                CustomerId = 2,
                Name = "Johnny's Pizza"
            },
            new Customer()
            {
                CustomerId = 2,
                Name = "Shifted ApS"
            }
        };
    }
}
