﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System.Data;
using System.Data.SqlClient;
using CuttingEdge.Conditions;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public class TaskMapper : DataMapperBase<Task>, ITaskMapper
    {
        public TaskMapper(ISqlConnectionFactory connectionFactory)
            : base(connectionFactory)
        {

        }

        public IList<Task> GetTasksForProject(int projectId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"
                    select *
                    from Tasks 
                    where ProjectId_FK = @projectId";

            cmd.Parameters.Add("@projectId", SqlDbType.Int).Value = projectId;

            return SelectObjects(cmd);
        }

        public override Task Get(int id)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = @"
                    select * 
                    from Tasks
                    where TaskId = @taskId";
            cmd.Parameters.Add("@taskId", SqlDbType.Int).Value = id;

            return SelectObject(cmd);
        }

        public override int Insert(Task data)
        {
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = @"
                    INSERT INTO Tasks
                         (ProjectId_FK, Name, Description, IsCompleted) 
                    values
                        (@ProjectId_FK, @Name, @Description, @IsCompleted) 

                    SELECT @@IDENTITY as NewRecordId
                    ";

            cmd.Parameters.Add("@ProjectId_FK", SqlDbType.Int).Value = data.ProjectId;
            cmd.Parameters.Add("@Name", SqlDbType.NVarChar, 140).Value = data.Name;
            cmd.Parameters.Add("@Description", SqlDbType.NVarChar, 1000).Value = base.Nullable(data.Description);
            cmd.Parameters.Add("@IsCompleted", SqlDbType.Bit).Value = data.IsCompleted;

            DataRow row = SelectSingleRow(cmd);
            return Convert.ToInt32(row["NewRecordId"]);
        }

        public override Task DataRowToModel(DataRow row)
        {
            Condition.Requires(row, "Task Data Row").IsNotNull();

            Task retVal = new Task();

            retVal.TaskId = base.DbRead<int>(row["TaskId"]);
            retVal.ProjectId = base.DbRead<int>(row["ProjectId_FK"]);
            retVal.Name = base.DbRead<string>(row["Name"]);
            retVal.Description = base.DbRead<string>(row["Description"]);
            retVal.IsCompleted = base.DbRead<bool>(row["IsCompleted"]);

            return retVal;
        }
    }
}
