﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public interface ITaskMapper : IDataMapper<Task>
    {
        IList<Task> GetTasksForProject(int projectId);
    }
}
