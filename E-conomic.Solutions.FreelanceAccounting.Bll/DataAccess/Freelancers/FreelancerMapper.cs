﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public class FreelancerMapper : DataMapperBase<Freelancer>, IFreelancerMapper
    {
        public FreelancerMapper(ISqlConnectionFactory connectionFactory)
            : base(connectionFactory)
        {

        }

        public Freelancer GetByEmailAddress(string emailAddress)
        {
            throw new NotImplementedException();
        }

        public override Freelancer Get(int id)
        {
            throw new NotImplementedException();
        }

        public override int Insert(Freelancer data)
        {
            throw new NotImplementedException();
        }

        public override Freelancer DataRowToModel(DataRow row)
        {
            throw new NotImplementedException();
        }

    }
}
