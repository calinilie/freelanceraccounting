﻿using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    interface IFreelancerMapper : IDataMapper<Freelancer>
    {
        Freelancer GetByEmailAddress(string emailAddress);
    }
}
