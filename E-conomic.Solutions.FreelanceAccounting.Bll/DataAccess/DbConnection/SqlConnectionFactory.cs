﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public class SqlConnectionFactory : ISqlConnectionFactory
    {
        private string _connectionString;
        public SqlConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public SqlConnection GetConnection()
        {
            SqlConnection retVal = new SqlConnection(_connectionString);
            retVal.Open();
            return retVal;
        }
    }
}
