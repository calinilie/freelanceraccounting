﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.DataAccess
{
    public interface IConnectionFactory<T>
        where T: IDbConnection
    {
        T GetConnection();
    }
}
