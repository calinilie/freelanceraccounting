﻿using Economic.Solutions.FreelanceAccounting.Bll.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.ServiceLayer
{
    public abstract class ServiceBase
    {
        protected IDataMappers _dataMappers;
        public ServiceBase(IDataMappers dataMappers)
        {
            _dataMappers = dataMappers;
        }
    }
}
