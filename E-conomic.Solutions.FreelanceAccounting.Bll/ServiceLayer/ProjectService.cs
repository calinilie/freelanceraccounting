﻿using Economic.Solutions.FreelanceAccounting.Bll.DataAccess;
using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.ServiceLayer
{

    public interface IProjectService
    {
        IList<Project> GetProjectsForCustomer(int customerId);
        Project GetProject(int projectId);
    }

    public class ProjectService : ServiceBase, IProjectService
    {

        public ProjectService(IDataMappers dataMappers)
            :base(dataMappers)
        {
            
        }

        public IList<Project> GetProjectsForCustomer(int customerId)
        {
            return _dataMappers.ProjectMapper.GetProjectsForCustomer(customerId);
        }

        public Project GetProject(int projectId)
        {
            return _dataMappers.ProjectMapper.Get(projectId);
        }
    }
}
