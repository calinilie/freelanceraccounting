﻿using Economic.Solutions.FreelanceAccounting.Bll.DataAccess;
using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.ServiceLayer
{
    public interface ITimeRegistrationService
    {
        TimeRegistration CreateTimeRegistration(TimeRegistration timeRegistration);
        IList<TimeRegistration> GetTimeRegistrationsForTask(int taskId, bool nonInvoicedOnly);
    }

    public class TimeRegistrationService : ServiceBase, ITimeRegistrationService
    {
        public TimeRegistrationService(IDataMappers dataMappers)
            :base(dataMappers)
        {

        }

        public TimeRegistration CreateTimeRegistration(TimeRegistration timeRegistration)
        {
            timeRegistration.TimeRegistrationId = _dataMappers.TimeRegistrationMapper.Insert(timeRegistration);
            return timeRegistration;
        }

        public IList<TimeRegistration> GetTimeRegistrationsForTask(int taskId, bool nonInvoicedOnly)
        {
            return _dataMappers.TimeRegistrationMapper.GetForTask(taskId, nonInvoicedOnly);
        }
    }
}
