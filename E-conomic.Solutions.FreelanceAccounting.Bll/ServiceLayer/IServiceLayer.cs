﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.ServiceLayer
{
    public interface IServices
    {
        ITaskService TaskService { get; }
        ITimeRegistrationService TimeRegistrationService { get; }
        ICustomerService CustomerService { get; }
        IProjectService ProjectService { get; }
    }
}
