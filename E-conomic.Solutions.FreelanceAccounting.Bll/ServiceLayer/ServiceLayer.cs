﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.ServiceLayer
{
    public class Services : IServices
    {
        private BllAutofacResolver _resolver;
        public Services(BllAutofacResolver resolver)
        {
            _resolver = resolver;
        }

        public ITaskService TaskService { get { return _resolver.Resolve<ITaskService>(); } }
        public IProjectService ProjectService { get { return _resolver.Resolve<IProjectService>(); } }
        public ICustomerService CustomerService { get { return _resolver.Resolve<ICustomerService>(); } }
        public ITimeRegistrationService TimeRegistrationService { get { return _resolver.Resolve<ITimeRegistrationService>(); } }
    }
}
