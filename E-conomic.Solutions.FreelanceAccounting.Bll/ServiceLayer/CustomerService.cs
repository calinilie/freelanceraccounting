﻿using Economic.Solutions.FreelanceAccounting.Bll.DataAccess;
using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Economic.Solutions.FreelanceAccounting.Bll.ServiceLayer
{
    public interface ICustomerService
    {
        Customer GetCustomer(int customerId);
        IList<Customer> GetAllCustomers();
    }


    public class CustomerService : ServiceBase, ICustomerService
    {
        public CustomerService(IDataMappers dataMappers)
            : base(dataMappers)
        {

        }

        public IList<Customer> GetAllCustomers()
        {
            return _dataMappers.CustomerMapper.GetAll();
        }

        public Customer GetCustomer(int customerId)
        {
            return _dataMappers.CustomerMapper.Get(customerId);
        }
    }
}
