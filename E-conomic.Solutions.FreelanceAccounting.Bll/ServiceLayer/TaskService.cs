﻿using Economic.Solutions.FreelanceAccounting.Bll.DataAccess;
using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Economic.Solutions.FreelanceAccounting.Bll.ServiceLayer
{
    public interface ITaskService
    {
        int CreateTask(Task task);
        IList<Task> GetTasksForProject(int projectId);
    }

    public class TaskService : ServiceBase, ITaskService
    {
        public TaskService(IDataMappers dataMappers)
            : base(dataMappers)
        {

        }

        public int CreateTask(Task task)
        {
            return _dataMappers.TaskMapper.Insert(task);
        }

        public IList<Task> GetTasksForProject(int projectId)
        {
            return _dataMappers.TaskMapper.GetTasksForProject(projectId);
        }
    }
}
