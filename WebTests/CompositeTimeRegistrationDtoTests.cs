﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Economic.Solutions.FreelanceAccounting.Web.Models;
using Economic.Solutions.FreelanceAccounting.Bll.DomainModel;

namespace WebTests
{
    [TestClass]
    public class CompositeTimeRegistrationDtoTests
    {
        [TestMethod]
        public void SetupFromModelsTest()
        {
            Project project = new Project()
            {
                ProjectId = 1,
                Name = "Rocket Design"
            };

            Task task = new Task()
            {
                TaskId = 2,
                Name = "Design Engines"
            };

            TimeRegistration timeReg = new TimeRegistration()
            {
                TimeRegistrationId = 4,
                StartDateTime = new DateTime(2015, 2, 20, 12, 0, 0),
                EndDateTime = new DateTime(2015, 2, 20, 16, 0, 0)
            };

            CompositeTimeRegistrationDto actual = new CompositeTimeRegistrationDto();
            actual.SetupFromModels(project, task, timeReg);

            Assert.AreEqual(project.ProjectId, actual.ProjectId);
            Assert.AreEqual(project.Name, actual.ProjectName);
            Assert.AreEqual(task.TaskId, actual.TaskId);
            Assert.AreEqual(task.Name, actual.TaskName);
            Assert.AreEqual(timeReg.TimeRegistrationId, actual.TimeRegistrationId);
            Assert.AreEqual(timeReg.Duration, actual.Duration);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))] 
        // this is not a good test! refactor the method so it throws a more concise exception
        public void SetupFromModelsTest2()
        {
            Task task = new Task()
            {
                TaskId = 2,
                Name = "Design Engines"
            };

            TimeRegistration timeReg = new TimeRegistration()
            {
                TimeRegistrationId = 4,
                StartDateTime = new DateTime(2015, 2, 20, 12, 0, 0),
                EndDateTime = new DateTime(2015, 2, 20, 16, 0, 0)
            };

            CompositeTimeRegistrationDto actual = new CompositeTimeRegistrationDto();
            actual.SetupFromModels(task, timeReg);
        }
    }
}
