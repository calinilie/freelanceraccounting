USE [FreelanceAccounting]
GO
/****** Object:  User [Freelancer]    Script Date: 03/13/2015 10:33:54 ******/
CREATE USER [Freelancer] FOR LOGIN [Freelancer] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[TimeRegistrations]    Script Date: 03/13/2015 10:33:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeRegistrations](
	[TimeRegistrationId] [int] IDENTITY(1,1) NOT NULL,
	[TaskId_FK] [int] NOT NULL,
	[FreelancerId_FK] [int] NOT NULL,
	[InvoiceId_FK] [int] NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NOT NULL,
	[Description] [nvarchar](140) NULL,
	[DocumentedProgressUrl] [nvarchar](2048) NULL,
 CONSTRAINT [PK_TimeRegistrations] PRIMARY KEY CLUSTERED 
(
	[TimeRegistrationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TimeRegistrations] ON
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (1, 1, 1, NULL, CAST(0x0000A454012482D0 AS DateTime), CAST(0x0000A45401457850 AS DateTime), N'Completed reading the homework assignment', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (2, 3, 1, NULL, CAST(0x0000A45601172575 AS DateTime), CAST(0x0000A456012FDD95 AS DateTime), N'This was inserted via integration tests', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (3, 2, 1, NULL, CAST(0x0000A456009450C0 AS DateTime), CAST(0x0000A45600AD08E0 AS DateTime), N'testing the API', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (4, 3, 1, NULL, CAST(0x0000A4560129791F AS DateTime), CAST(0x0000A4560142313F AS DateTime), N'This was inserted via integration tests', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (5, 2, 1, NULL, CAST(0x0000A456009450C0 AS DateTime), CAST(0x0000A45600AD08E0 AS DateTime), N'testing the API', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (6, 12, 1, NULL, CAST(0x0000A457011826C0 AS DateTime), CAST(0x0000A45701391C40 AS DateTime), N'Have been doing some thinking', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (7, 14, 1, NULL, CAST(0x0000A457013D3AF0 AS DateTime), CAST(0x0000A45701457850 AS DateTime), N'launched instance, had to install .net 4.5 -> would take too long, aborted', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (8, 16, 1, NULL, CAST(0x0000A45701499700 AS DateTime), CAST(0x0000A457014DB5B0 AS DateTime), N'created ELB environment', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (9, 15, 1, NULL, CAST(0x0000A2EF00A4CB80 AS DateTime), CAST(0x0000A2EF00D63BC0 AS DateTime), N'meeting went good, got an idea about requirements.', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (10, 16, 1, NULL, CAST(0x0000A45701499700 AS DateTime), CAST(0x0000A4570151D460 AS DateTime), N'install AWS toolkit for VS 2013', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (11, 16, 1, NULL, CAST(0x0000A457015A11C0 AS DateTime), CAST(0x0000A45701624F20 AS DateTime), N'application deployed', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (12, 2, 1, NULL, CAST(0x0000A45800B444FF AS DateTime), CAST(0x0000A45800CCFD1F AS DateTime), N'This was inserted via integration tests', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (13, 16, 1, NULL, CAST(0x0000A41300000000 AS DateTime), CAST(0x0000A41400000000 AS DateTime), N'Tester', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (14, 17, 1, NULL, CAST(0x0000A458015A11C0 AS DateTime), CAST(0x0000A4580172C9E0 AS DateTime), N'have prepared the presentation', NULL)
INSERT [dbo].[TimeRegistrations] ([TimeRegistrationId], [TaskId_FK], [FreelancerId_FK], [InvoiceId_FK], [StartDateTime], [EndDateTime], [Description], [DocumentedProgressUrl]) VALUES (15, 21, 1, NULL, CAST(0x0000A11C009450C0 AS DateTime), CAST(0x0000A11C00C5C100 AS DateTime), N'have met with E-conomic', NULL)
SET IDENTITY_INSERT [dbo].[TimeRegistrations] OFF
/****** Object:  Table [dbo].[Tasks]    Script Date: 03/13/2015 10:33:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId_FK] [int] NOT NULL,
	[Name] [nvarchar](140) NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[IsCompleted] [bit] NOT NULL,
 CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Tasks] ON
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (1, 1, N'Read assignment', N'in order to undestand what you have to do, you need to read it first, mkay?!', 1)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (2, 4, N'Test Task', N'Task inserted from integration tests', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (3, 4, N'Test Task', N'Task inserted from integration tests', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (4, 4, N'Test Task', N'Task inserted from integration tests', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (5, 4, N'Test Task', N'Task inserted from integration tests', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (6, 4, N'Test Task', N'Task inserted from integration tests', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (7, 4, N'Test Task', N'Task inserted from integration tests', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (8, 4, N'test', N'testing the API', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (9, 4, N'Test Task', N'Task inserted from integration tests', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (10, 4, N'Test Task', N'Task inserted from integration tests', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (11, 4, N'Test Task', N'Task inserted from integration tests', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (12, 1, N'think about the assignment', NULL, 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (13, 1, N'identify requirements', NULL, 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (14, 2, N'Setup EC2 instance', NULL, 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (15, 3, N'Have a meeting with client', NULL, 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (16, 2, N'deploy on Elastic beanstalk', NULL, 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (17, 1, N'Prepare presentation of solution', NULL, 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (18, 4, N'Test Task', N'Task inserted from integration tests', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (19, 2, N'T2', NULL, 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (20, 3, N'Tester', N'This is a test', 0)
INSERT [dbo].[Tasks] ([TaskId], [ProjectId_FK], [Name], [Description], [IsCompleted]) VALUES (21, 1, N'Make app presentation', N'description', 0)
SET IDENTITY_INSERT [dbo].[Tasks] OFF
/****** Object:  Default [DF__Tasks__IsComplet__1A14E395]    Script Date: 03/13/2015 10:33:54 ******/
ALTER TABLE [dbo].[Tasks] ADD  DEFAULT ((0)) FOR [IsCompleted]
GO
